SET(DLL_NAME IMGUI)

message(STATUS "Adding ${DLL_NAME} library")

# --------------------------
# Setup sources
# --------------------------
SET (IMGUI_INCLUDE_PATH ${CMAKE_CURRENT_LIST_DIR})
SET (IMGUI_SRC_PATH ${CMAKE_CURRENT_LIST_DIR})

SET (IMGUI_HEADERS
				"${IMGUI_INCLUDE_PATH}/imconfig.h"
				"${IMGUI_INCLUDE_PATH}/imgui.h"
				"${IMGUI_INCLUDE_PATH}/imgui_internal.h"
				"${IMGUI_INCLUDE_PATH}/imstb_rectpack.h"
				"${IMGUI_INCLUDE_PATH}/imstb_textedit.h"
				"${IMGUI_INCLUDE_PATH}/imstb_truetype.h"
				)
				
SET (IMGUI_SOURCES
				"${IMGUI_SRC_PATH}/imgui.cpp"
				"${IMGUI_SRC_PATH}/imgui_demo.cpp"
				"${IMGUI_SRC_PATH}/imgui_draw.cpp"
				"${IMGUI_SRC_PATH}/imgui_widgets.cpp"
				)
				
# --------------------------
# Setup library
# --------------------------
add_library(${DLL_NAME} STATIC
				${IMGUI_HEADERS}
				${IMGUI_SOURCES})
				
# --------------------------
# Setup source groups
# --------------------------
SET (IMGUI_INCLUDE_SOURCEGROUP include)

SET (IMGUI_SRC_SOURCEGROUP src)

source_group("${IMGUI_INCLUDE_SOURCEGROUP}" FILES ${IMGUI_HEADERS})

source_group("${IMGUI_SRC_SOURCEGROUP}" FILES ${IMGUI_SOURCES})

# --------------------------
# Folder structure in the IDE
# --------------------------
set_target_properties(${DLL_NAME} PROPERTIES   FOLDER          "0. Externals"
								  DEFINE_SYMBOL   "")
														
# --------------------------------
# Setup working directory
# --------------------------------
if(NOT ${CMAKE_VERSION} VERSION_LESS 3.8)
set_target_properties( ${DLL_NAME} PROPERTIES VS_DEBUGGER_WORKING_DIRECTORY "${OUTPUT_BINDIR}")
else()
message(WARNING "CMake version: ${CMAKE_VERSION}.\n" 
                "Only CMake version from 3.8 or higher are able to set the working directory.\n"
                "Content directory will not be copied, you should copy the content directory.\n"
                    "from:\n${PROJECT_SOURCE_DIR}/${CONTENT_DIR}\n"
                    "to:\n${PROJECT_BINARY_DIR}/${APPLICATIONS_DIR}/ \"your_application_name_here\" /${REX_BUILD_TARGET}/${CONTENT}")
endif()

# --------------------------
# Target include directories.
# --------------------------
target_include_directories(${DLL_NAME} PUBLIC     
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}}>)
target_include_directories(${DLL_NAME} PUBLIC     
    $<BUILD_INTERFACE:${CMAKE_CURRENT_LIST_DIR}>
    $<INSTALL_INTERFACE:${CMAKE_INSTALL_INCLUDEDIR}>)